package tests;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

//import edu.wit.cs.comp1000.tests.PA3bTestCase.Sign;
//import edu.wit.cs.comp1000.PA3b;
//import edu.wit.cs.comp1000.tests.TestSuite;
//import edu.wit.cs.comp1000.PA3a;
import junit.framework.TestCase;
import orientation.Orientation;

public class OrientationTest extends TestCase {
	
	private void _test(String values, String result) {
		final String input = values;
		
		final String output =  result ;
		
		final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setIn(new ByteArrayInputStream(input.getBytes()));
		System.setOut(new PrintStream(outContent));
		
		Orientation.main(new String[] { "foo" });
        {
				assertEquals("Enter Name: "+ output, outContent.toString());

		
		System.setIn(null);
		System.setOut(null);
	}
	}
	
	private void _testResult(String name, String advisor) {
		_test(name,advisor);
	}
	
//	public void testOudshoorn() {
//		_testResult("X", "Oudshoorn");
//		_testResult("XX", "Oudshoorn");
//	    }
	
	
	
	public void testElibidy() {
	_testResult("aaa", "Elibidy");
	_testResult("dma", "Elibidy");
}	
	
	public void testWiseman() {
		_testResult("dmb", "Wiseman");
		_testResult("hma", "Wiseman");
	}		
		
	public void testDerbinsky() {
	_testResult("hmb", "Derbinsky");
	_testResult("kma", "Derbinsky");
    }
	
	public void testRusso() {
		_testResult("kmb", "Russo");
		_testResult("nma", "Russo");			
	    }
	
	public void testMacLean() {
		_testResult("nmb", "MacLean");
		_testResult("qma", "MacLean");
	    }
	public void testWu() {
		_testResult("qmb", "Wu");
	    }
	public void testYun() {
		_testResult("tmb", "Yun");
		_testResult("wma", "Yun");
	    }
	public void testSuresh() {
		_testResult("wmb", "Suresh");
		_testResult("zzz", "Suresh");
	    }
	
	
	
	
	

}	
